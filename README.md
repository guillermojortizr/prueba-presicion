
## Prueba PRESICION - Guillermo Ortiz

Esta prueba fue realizada sobre docker, por favor asegurese de tener instalado docker y docker-compose.

1. Clone el repositorio.
2. Cambie de directorio a la raiz del proyecto y ejecute "docker-compose up --build".
3. Esto creara el contenedor el cual en su interior tiene 3 servicios.
4. La aplicación es accedida desde [localhost:4200](http://localhost:4200).
5. Otro de los servicios es phpMyAdmin servido en  [localhost:8000](http://localhost:8000).
6. Por último el servicio para MySQL para accedida desde "localhost:3306"

---
*Nota: Los datos de la base de datos son copiados automáticamente*