<?php
/**
 * Crea la Conección a la base de datos
 */
function get_connection () {
  $server_name = "db";
  $user_name = "root";
  $password = $_ENV["MYSQL_ROOT_PASSWORD"];
  $db_name = $_ENV["MYSQL_DATABASE"];
  
  $conn = mysqli_connect($server_name, $user_name, $password, $db_name, 3306);
  mysqli_set_charset($conn,'utf8mb4');
  if (!$conn) {
    die('No se pudo conectar a MySQL: ' . mysqli_connect_error());
  }
  return $conn;
}


/**
 * Retorna la cantidad de registros
 */
function countRows ($keyword = NULL) {
  $conn = get_connection();
  $query = "SELECT COUNT(*) FROM `product`";
  if ($keyword) {
    $query .= " WHERE `title` LIKE '%" . $keyword . "%'";
  }
  $result = mysqli_query($conn, $query);
  $data = mysqli_fetch_array($result);
  return (int) $data[0];
}

/**
 * Proceso de busqueda por palabra clave
 */
function search ($keyword = NULL, $page = 1, $limit = 10) {
  $conn = get_connection();
  $query = "SELECT * FROM `product`";

  if ($keyword) {
    $query .= " WHERE `title` LIKE '%$keyword%'";
  }

  $query .= " ORDER BY `title` ASC";

  if ($limit) {
    $query .= " LIMIT $limit";
  }
  
  $total_rows = countRows($keyword);
  $offset = ($page - 1) * $limit;
  if ($total_rows > $offset) {
    $query .= " OFFSET $offset";
  }
  
  $result = mysqli_query($conn, $query);
  $data = [];
  if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      array_push($data, $row);
    }
  }

  return json_encode([
    'data' => $data,
    'total_rows' => $total_rows,
    'previous_page' => $page - 1,
    'actual_page' => $page,
    'next_page' => $page + 1,
    'total_pages' => ceil($total_rows / $limit)
    ]);
}

// Punto de entrada
$keyword = $_POST["search"];
$page = $_POST["page"];
echo search($keyword, $page);
