
// Carga los productos en el contenedor
const loadProducts = (products) => {
  $('#containerProducts').html('');
  products.forEach(element => {
    const row = $(`<tr>
      <th scope="row">${element.id}</th>
      <td>${element.title}</td>
      <td>${element.description}</td>
      <td>${element.price}</td>
      <td>${element.sold_out}</td>
    </tr>`);
    $('#containerProducts').append(row);
    $('#containerProducts').trigger("create");
  });
}

// Carga los botones de paginación
const loadPagination = ({ actual_page, next_page, previous_page, total_pages, total_rows }) => {
  $('#pagination').html('');
  $('#pagination').append($('<li class="page-item"><button class="page-link" id="first"><span>&laquo;</span></button></li>'));
  if (previous_page > 0) {
    $('#pagination').append($(`<li class="page-item"><button class="page-link" id="previous">${previous_page}</button></li>`));
  }
  $('#pagination').append($(`<li class="page-item"><button class="page-link" id="actual">${actual_page}</button></li>`));
  if (total_pages > next_page) {
    $('#pagination').append($(`<li class="page-item"><button class="page-link" id="next">${next_page}</button></li>`));
    $('#pagination').append($(`<li class="page-item"><button class="page-link" id="last"><span>...${total_pages}</span></button></li>`));
  }

  $('#pagination').trigger("create");

  $('#first').on('click', search);
  $('#previous').on('click', search);
  $('#actual').on('click', search);
  $('#next').on('click', search);
  $('#last').on('click', search);
}

// Proceso en case de éxito
const success = (response) => {
  response = JSON.parse(response);
  const products = response.data;
  loadProducts(products);
  loadPagination(response);
}

// Proceso principal
const search = (event) => {
  const searchInput = document.querySelector('#searchInput');
  const data = { search: searchInput.value, page: 1 };

  if (event.key && event.key !== "Enter") {
    return
  }

  if (event && event.target && event.target.id) {
    switch (event.target.id) {
      case 'first':
        data.page = 1;
        break;
      case 'previous':
        data.page = Number(event.target.innerText);
        break;
      case 'actual':
        return;
      case 'next':
        data.page = Number(event.target.innerText);
        break;
      case 'last':
        data.page = Number(event.target.innerText.replace(/\./g, ''));
        break;
      default:
        break;
    }
  }

  $.ajax({
    url: 'search.php',
    method: 'POST',
    data,
    success
  })
}

// Escuchas
const searchButton = document.querySelector('#searchButton');
searchButton.addEventListener('click', search);
document.addEventListener('keypress', search);

// Se cargan los primeros productos
$(document).ready(search);